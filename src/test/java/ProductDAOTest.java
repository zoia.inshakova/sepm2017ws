import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.persistence.ProductDAO;
import at.ac.tuwien.sepm.assignment.individual.persistence.ProductDAOimpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by zoiainshakova on 17/11/2017.
 *
 * @author zoiainshakova
 */
public class ProductDAOTest {

    private ProductDAO productDAO;

    @Before
    public void setup(){
        productDAO = new ProductDAOimpl();
    }

    @Test
    public void product_create_valid(){
        Product p = new Product();
        p.setName("TestName");
        p.setDescription("TestDescription");
        p.setNettoPrice(12);

        productDAO.create(p);

        List<Product> list = productDAO.getallProducts();


    }

}
