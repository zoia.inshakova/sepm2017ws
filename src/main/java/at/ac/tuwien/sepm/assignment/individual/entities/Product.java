package at.ac.tuwien.sepm.assignment.individual.entities;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Product {

    private IntegerProperty id;
    private StringProperty name;
    private StringProperty description;
    private IntegerProperty nettoPrice;
    private StringProperty picture;
    private ObjectProperty<LocalDateTime> timeCreated;
    private ObjectProperty<LocalDateTime> timeLastModified;
    private BooleanProperty isDeleted;
    private StringProperty category;
    private StringProperty taxClass;



    /**
     * Default constructor.
     */
    public Product() {
        this(null);
    }


    /**
     * Constructor with some initial data.
     *
     * @param name

     */
    public Product(String name) {


        this.name = new SimpleStringProperty(name);
        // some initial dummy data
        this.nettoPrice = new SimpleIntegerProperty(200);
        this.description = new SimpleStringProperty("description");
        this.picture = new SimpleStringProperty("picture path");
        this.timeCreated = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(2017,10,10,01,22,22));
        this.timeLastModified = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(2017,11,10,01,33,33));
        this.isDeleted = new SimpleBooleanProperty(false);
        this.category = new SimpleStringProperty("starter");
        this.taxClass = new SimpleStringProperty("10%");
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id= new SimpleIntegerProperty(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public int getNettoPrice() {
        return nettoPrice.get();
    }

    public IntegerProperty nettoPriceProperty() {
        return nettoPrice;
    }

    public void setNettoPrice(int nettoPrice) {
        this.nettoPrice.set(nettoPrice);
    }

    public String getPicture() {
        return picture.get();
    }

    public StringProperty pictureProperty() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture.set(picture);
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated.get();
    }

    public ObjectProperty<LocalDateTime> timeCreatedProperty() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated.set(timeCreated);
    }

    public LocalDateTime getTimeLastModified() {
        return timeLastModified.get();
    }

    public ObjectProperty<LocalDateTime> timeLastModifiedProperty() {
        return timeLastModified;
    }

    public void setTimeLastModified(LocalDateTime timeLastModified) {
        this.timeLastModified.set(timeLastModified);
    }

    public boolean isIsDeleted() {
        return isDeleted.get();
    }

    public BooleanProperty isDeletedProperty() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted.set(isDeleted);
    }

    public String getCategory() {
        return category.get();
    }

    public StringProperty categoryProperty() {
        return category;
    }

    public void setCategory(String category) {
        this.category.set(category);
    }

    public String getTaxClass() {
        return taxClass.get();
    }

    public StringProperty taxClassProperty() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass.set(taxClass);
    }
}
