package at.ac.tuwien.sepm.assignment.individual.universe.service;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.persistence.ProductDAO;

public class SimpleServiceImpl implements SimpleService {

    private final ProductDAO productDAO;

    public SimpleServiceImpl (ProductDAO productDAO){
        this.productDAO = productDAO;
    }

    @Override
    public void create(Product product) {
        productDAO.create(product);
    }
}