package at.ac.tuwien.sepm.assignment.individual.util;


import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseSingleton {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static String db_url = "jdbc:h2";
    private static String db_name = "test";
    private static  String db_user = "sa";
    private static  String db_password = "";
    private static Connection connection = null;



    public static Connection setConnection() {

        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            String url = "" + db_url + ":" + "~/" + db_name + ";INIT=RUNSCRIPT FROM 'classpath:sql/createAndInsert.sql'";
            LOG.debug(url);
            return DriverManager.getConnection(url, db_user, db_password);


        } catch (SQLException ex) {
            ex.printStackTrace();

        }
        return null;

    }

    public static Connection getConnetion() {
        if (connection==null){
            connection = setConnection();
        }
        return connection;

    }

    public static void closeConnection(){
        if (connection!=null){
            try{connection.close();
            }catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }
}