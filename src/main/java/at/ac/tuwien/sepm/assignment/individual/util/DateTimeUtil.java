package at.ac.tuwien.sepm.assignment.individual.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Helper functions for handling dates.
 */

public class DateTimeUtil {

    /** The date pattern that is used for conversion */
    private static final String DATE_TIME_PATTERN = "yyyy/MM/dd HH:mm:ss";

    /** The date formatter*/

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

        /**
         * Returns the given date as a well formatted String. The above defined
         * {@link DateTimeUtil#DATE_TIME_PATTERN} is used.
         *
         * @param date_time the date to be returned as a string
         * @return formatted string
         */
        public static String format(LocalDateTime date_time) {
            if (date_time == null) {
                return null;
            }
            return DATE_TIME_FORMATTER.format(date_time);
        }

        /**
         * Converts a String in the format of the defined {@link DateTimeUtil#DATE_TIME_PATTERN}
         * to a {@link java.time.LocalDateTime} object.
         *
         * Returns null if the String could not be converted.
         *
         * @param dateTimeString the date as String
         * @return the date object or null if it could not be converted
         */

        public static LocalDateTime parse(String dateTimeString) {
            try {
                return DATE_TIME_FORMATTER.parse(dateTimeString, LocalDateTime::from);
            } catch (DateTimeParseException e) {
                return null;
            }
        }

        /**
         * Checks the String whether it is a valid date.
         *
         * @param dateTimeString
         * @return true if the String is a valid date
         */
        public static boolean validDate(String dateTimeString) {
            // Try to parse the String.
            return DateTimeUtil.parse(dateTimeString) != null;
        }
    }

