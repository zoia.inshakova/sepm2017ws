package at.ac.tuwien.sepm.assignment.individual.application;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;

import at.ac.tuwien.sepm.assignment.individual.persistence.ProductDAO;
import at.ac.tuwien.sepm.assignment.individual.persistence.ProductDAOimpl;
import at.ac.tuwien.sepm.assignment.individual.universe.service.SimpleService;
import at.ac.tuwien.sepm.assignment.individual.universe.service.SimpleServiceImpl;
import at.ac.tuwien.sepm.assignment.individual.universe.ui.ProductEditDialogController;
import at.ac.tuwien.sepm.assignment.individual.universe.ui.ProductOverviewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

import javafx.collections.ObservableList;

import static javafx.collections.FXCollections.*;



public final class MainApplication extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;


    /**
     * The data as an observable list of Product.
     */
   // private ObservableList<Product> productData = observableArrayList();

    /**
     * Constructor
     */
   /* public MainApplication() {
        // Add some sample data
        productData.add(new Product("Coffee"));
        productData.add(new Product("Tea"));
        productData.add(new Product("Milk"));
        productData.add(new Product("Pasta"));

    }

*/
    /**
     * Returns the data as an observable list of Persons.
     * @return
     */
 /*   public ObservableList<Product> getProductData() {
        return productData;
    }
*/

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Restaurant");

       initRootLayout();
       showProductOverview();


    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApplication.class.getResource("/fxml/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showProductOverview() {

        try {
            // Load product overview
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApplication.class.getResource("/fxml/ProductOverview.fxml"));
            AnchorPane productOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(productOverview);

            // Give the controller access to the main app.

            ProductDAO productDAO = new ProductDAOimpl();
            SimpleService simpleService = new SimpleServiceImpl(productDAO);

           ProductOverviewController controller = loader.getController();
           controller.setMainApplication(this, simpleService);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }


    public boolean showProductEditDialog(Product product) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApplication.class.getResource("/fxml/ProductEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Product");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the product into the controller.
            ProductEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setProduct(product);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }



    public static void main(String[] args) {

        launch(args);
    }
}


