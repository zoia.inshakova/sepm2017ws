package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.util.DataBaseSingleton;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAOimpl implements ProductDAO {

    private String insertProduct="INSERT into PRODUCT (name,description,netto_price) values(?,?,?)" ;
    private String getAllProducts= "SELECT * from PRODUCT";

    @Override
    public Product create(Product product) {

        PreparedStatement stm;
        ResultSet rset;

        try {
            Connection con = DataBaseSingleton.getConnetion();
            stm = con.prepareStatement(insertProduct, Statement.RETURN_GENERATED_KEYS);
            stm.setString(1, product.getName());
            stm.setString(2, product.getDescription());
            stm.setInt(3,product.getNettoPrice());

            stm.executeUpdate();
            rset = stm.getGeneratedKeys();
            if(rset.next()){
                product.setId(rset.getInt(1));
            }

        }

        catch(SQLException ex){
            ex.printStackTrace();

        }
        return null;
    }

    @Override
    public List<Product> getallProducts() {

        PreparedStatement stm;
        ResultSet rset;
        List<Product> allProducts = new ArrayList<Product>();

        try {
            Connection con = DataBaseSingleton.getConnetion();
            stm = con.prepareStatement(getAllProducts, Statement.RETURN_GENERATED_KEYS);
            stm.executeUpdate();
            rset = stm.getGeneratedKeys();

            while(rset.next()) {
                Product p = new Product();
                p.setId(rset.getInt("id"));
                p.setName(rset.getString("name"));
                p.setDescription(rset.getString("description"));
                p.setNettoPrice(rset.getInt("netto_price"));
                p.setPicture(rset.getString("picture"));
                p.setCategory(rset.getString("category"));
                p.setIsDeleted(rset.getBoolean("is_deleted"));
                p.setTaxClass(rset.getString("tax_class"));
                p.setTimeCreated(rset.getTimestamp("time_created").toLocalDateTime());
                p.setTimeLastModified(rset.getTimestamp("time_last_modified").toLocalDateTime());
                allProducts.add(p);
            }
        }

        catch(SQLException ex){
            ex.printStackTrace();

        }
        return allProducts;
    }
}
