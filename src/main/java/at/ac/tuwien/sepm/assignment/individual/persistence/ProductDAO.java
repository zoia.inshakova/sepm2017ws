package at.ac.tuwien.sepm.assignment.individual.persistence;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import java.util.List;



public interface ProductDAO {
    /**
     * Creation of the product
     * @param product - the product that should be created in the method
     * @return - the created product
     */
    Product create(Product product);

    /**
     * returns a list of all products
     * @return - list of type product
     */
    List <Product> getallProducts();
}
