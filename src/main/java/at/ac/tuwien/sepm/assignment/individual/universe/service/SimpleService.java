package at.ac.tuwien.sepm.assignment.individual.universe.service;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;

public interface SimpleService {

    void create(Product product);

}
