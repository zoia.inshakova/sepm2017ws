package at.ac.tuwien.sepm.assignment.individual.universe.ui;
import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * Dialog to edit details of a product

 */

public class ProductEditDialogController {


    @FXML
    private TextField nameField;
    @FXML
    private TextField descriptionField;
    @FXML
    private TextField nettoPriceField;
    @FXML
    private TextField pictureField;
    @FXML
    private TextField categoryField;
    @FXML
    private TextField taxClassField;


    private Stage dialogStage;
    private Product product;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param product
     */
    public void setProduct(Product product) {
        this.product = product;

        nameField.setText(product.getName());
        descriptionField.setText(product.getDescription());
        nettoPriceField.setText(Integer.toString(product.getNettoPrice()));
        pictureField.setText(product.getPicture());
        categoryField.setText(product.getCategory());
        taxClassField.setText(product.getTaxClass());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOK() {
        if (isInputValid()) {
            product.setName(nameField.getText());
            product.setDescription(descriptionField.getText());
            product.setNettoPrice(Integer.parseInt(nettoPriceField.getText()));
            product.setPicture(pictureField.getText());
            product.setCategory(categoryField.getText());
            product.setTaxClass(taxClassField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "Please enter the name of the product\n";
        }
        if (descriptionField.getText() == null || descriptionField.getText().length() == 0) {
            errorMessage += "Please enter the description of a product\n";
        }

        if (nettoPriceField.getText() == null || nettoPriceField.getText().length() == 0) {
            errorMessage += "Please enter the netto price\n";
        } else {
            // try to parse the postal code into an int.
            try {
                Integer.parseInt(nettoPriceField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "This field should be of type integer\n";
            }
        }

        if (pictureField.getText() == null || pictureField.getText().length() == 0) {
            errorMessage += "Please enter the picture address\n";
        }

// ADD OPTIONS

        if (categoryField.getText() == null || categoryField.getText().length() == 0) {
            errorMessage += "Please enter the category\n";
        }

        if (taxClassField.getText() == null || taxClassField.getText().length() == 0) {
            errorMessage += "Please enter the tax class\n";
        }



        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
