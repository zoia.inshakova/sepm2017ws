package at.ac.tuwien.sepm.assignment.individual.universe.ui;

import at.ac.tuwien.sepm.assignment.individual.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.universe.service.SimpleService;
import at.ac.tuwien.sepm.assignment.individual.util.DateTimeUtil;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import at.ac.tuwien.sepm.assignment.individual.application.MainApplication;
import javafx.scene.control.Alert.AlertType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;


public class ProductOverviewController {


    private ObservableList<Product> masterData = FXCollections.observableArrayList();

    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Product, String> productColumn;
    @FXML
    private TableColumn<Product, String> categoryColumn;
    @FXML
    private TableColumn<Product, String> nettoPriceColumn;
    @FXML
    private TableColumn<Product, String> pictureColumn;



    @FXML
    private Label nameLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label nettoPriceLabel;
    @FXML
    private Label pictureLabel;

    @FXML
    private Label categoryLabel;

    @FXML
    private Label taxClassLabel;

    @FXML
    private Label timeCreatedLabel;

    @FXML
    private Label timeLastModifiedLabel;


    @FXML
    private TextField filterField;

    // Reference to the main application.
    private MainApplication mainApplication;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ProductOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */

    @FXML
    private void initialize() {

        // Initialize the product table with 1 column
        productColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        categoryColumn.setCellValueFactory(cellData -> cellData.getValue().categoryProperty());
        nettoPriceColumn.setCellValueFactory(cellData -> cellData.getValue().nettoPriceProperty().asString());
        pictureColumn.setCellValueFactory(cellData -> cellData.getValue().pictureProperty());

        // Clear product details.
        showProductDetails(null);

        // Listen for selection changes and show the product details when changed.
        productTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> showProductDetails(newValue));

    //creating a search filter

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Product> filteredData = new FilteredList<>(masterData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(product -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare  name and category of every product with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (product.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches name
                } else if (product.getCategory().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches category
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Product> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(productTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        productTable.setItems(sortedData);
    }



    private SimpleService simpleService;



    /**
     * Is called by the main application to give a reference back to itself.
     * @param mainApplication
     */
    public void setMainApplication(MainApplication mainApplication, SimpleService simpleService) {
        this.mainApplication = mainApplication;
        this.simpleService = simpleService;

        // Add observable list data to the table
       // productTable.setItems(mainApplication.getProductData());
    }

    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param product the person or null
     */
    private void showProductDetails(Product product) {
        if (product != null) {
            // Fill the labels with info from the person object.
            nameLabel.setText(product.getName());
            descriptionLabel.setText(product.getDescription());
            pictureLabel.setText(product.getPicture());
            categoryLabel.setText(product.getCategory());
            taxClassLabel.setText(product.getTaxClass());
            nettoPriceLabel.setText(Integer.toString(product.getNettoPrice()));
            timeCreatedLabel.setText(DateTimeUtil.format(product.getTimeCreated()));
            timeLastModifiedLabel.setText(DateTimeUtil.format(product.getTimeLastModified()));

        } else {
            // Product is null, remove all the text.
            nameLabel.setText("");
            descriptionLabel.setText("");
            pictureLabel.setText("");
            categoryLabel.setText("");
            taxClassLabel.setText("");
            nettoPriceLabel.setText("");
            timeCreatedLabel.setText("");
            timeLastModifiedLabel.setText("");
        }
    }

    @FXML
    private void handleDeleteProduct() {
        int selectedIndex = productTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            productTable.getItems().remove(selectedIndex);
        } else

        {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApplication.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Product Selected");
            alert.setContentText("Please select a product in the table.");

            alert.showAndWait();
        }


    }

    /**
     * Called when the user clicks the new button.
     * Opens a dialog to edit details for a new person.
     */

    @FXML
    private void handleAddProduct() {
        Product tempProduct = new Product();
        boolean okClicked = mainApplication.showProductEditDialog(tempProduct);
        if (okClicked) {
            simpleService.create(tempProduct);
          //  mainApplication.getProductData().add(tempProduct);
        }
    }

    /**
     * Called when the user clicks the edit button.
     * Opens a dialog to edit details for the selected person.
     */

    @FXML
    private void handleEditProduct() {
        Product selectedProduct = productTable.getSelectionModel().getSelectedItem();
        if (selectedProduct != null) {
            boolean okClicked = mainApplication.showProductEditDialog(selectedProduct);
            if (okClicked) {
                showProductDetails(selectedProduct);
            }

        } else {
            // Nothing selected
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApplication.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Product Selected");
            alert.setContentText("Please select a product in the table.");

            alert.showAndWait();
        }
    }
}



