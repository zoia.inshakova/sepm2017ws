
CREATE TABLE if not exists Product

(ID bigint auto_increment not null primary key,
 name varchar(50) not null,
 description varchar (200),
 netto_price int not null,
 picture varchar (300),
 time_created timestamp default current_timestamp,
 time_last_modified timestamp,
 is_deleted boolean,
 category varchar (50),
 tax_class varchar(50)

);

CREATE TABLE if not exists Invoice
(ID bigint auto_increment not null primary key,
 unique_number bigint auto_increment not null,
 status BOOLEAN not null,
 table_number int not null,
 time_created timestamp default current_timestamp,
 time_last_modified timestamp,
 payment_means varchar(50)


);

CREATE TABLE if not exists ProductInInvoice
(productID bigint REFERENCES Product(ID),
 invoiceID bigint REFERENCES Invoice(ID),
 time_added TIMESTAMP,
 quantity int not null,
  primary key (productID,invoiceID)
);